# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError

from odoo.addons.website.tools import get_video_embed_code


class InfrastructuresImage(models.Model):
    _name = 'infrastructures.image'
    _description = "Infrastructure Image"
    _inherit = ['image.mixin']
    _order = 'sequence, id'

    name = fields.Char("Name", required=True)
    sequence = fields.Integer(default=10, index=True)
    description = fields.Text("Description")
    image_1920 = fields.Image(required=True)

    infrastructure_id = fields.Many2one('infrastructures.infrastructure', "Infrastructure ", index=True, ondelete='cascade')
    @api.depends('image_1920', 'image_1024')
    def _compute_can_image_1024_be_zoomed(self):
        for image in self:
            image.can_image_1024_be_zoomed = image.image_1920 and tools.is_image_size_above(image.image_1920, image.image_1024)

    @api.model
    def create(self, vals):
        if vals.get('sequence','New') == 'New':
            vals['sequence'] = self.env['ir.sequence'].next_by_code('infrastructures.infrastructure.image.sequence') or 'New'
        result = super(InfrastructuresImage, self).create(vals)
        return result  
    # @api.model_create_multi
    #     def create(self, vals_list):
    #         normal_vals = []
    #         for vals in vals_list:
    #             normal_vals.append(vals)
    #         return super().create(normal_vals)