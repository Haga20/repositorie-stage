from odoo import fields, api, models
class PurchaseRequisition(models.Model):
    _inherit='purchase.requisition'
    
    operation_id = fields.Many2one('infrastructures.operation', string="Operation")