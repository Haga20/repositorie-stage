from odoo import models,fields,api, _
from odoo.exceptions import ValidationError

class Operation(models.Model):
    _name = 'infrastructures.operation'
    _description = 'operation model'

    name = fields.Char(string="Designation", required = True)
    sequence = fields.Char(string='Operation ID', required=True, copy=False, readonly=True, index=True, default=lambda self: 'New')
    description = fields.Text(string= "Description", required=True)
    infrastructures_id = fields.Many2one('infrastructures.infrastructure', string='Infrastructure', hide=True)
    stock_operation_type = fields.Many2one('stock.picking.type', string='Stock Operation type', required=True, domain="[('code', '=', 'outgoing')]")
    state = fields.Selection([
        ('new', 'New'),
        ('confirmed', 'Confirmed'),
        ('inprogress', 'In progress'),
        ('done', 'Done'),
        ('canceled', 'Canceled'),
    ], string='Operation status', readonly=True, default='new')
    article_line_ids = fields.One2many('infrastructures.operations.article', 'operation_id', string="Product line")
    worker_line_ids = fields.One2many('infrastructures.operations.worker', 'operation_id', string="Worker line")
    purchase_requisition_line_ids = fields.One2many('purchase.requisition', 'operation_id', string="Purchase requisition")
    
    def open_operations_purchase_requisition(self):
        if not self.env.user.has_group('base.group_infrastructure_dept'):
            raise ValidationError(_("Only the infrastructure member can access the purchase requisition "))
        return{
            'name':'Purchase requisition',
            'domain':[('operation_id','=',self.id)],
            'view_type':'form',
            'res_model':'purchase.requisition',
            'view_id':False,
            'view_mode':'tree,form',
            'type':'ir.actions.act_window',
            'context': {
                "default_operation_id": self.id,
            },
        }
    def get_purchase_requisition_number(self):
        number = self.env['purchase.requisition'].search_count([('operation_id.id', '=', self.id)])
        self.purchase_requisition_number = number

    purchase_requisition_number = fields.Integer(string='Purchase requisition', compute='get_purchase_requisition_number')
   
    @api.model
    def create(self, vals):
        if vals.get('sequence','New') == 'New':
            vals['sequence'] = self.env['ir.sequence'].next_by_code('infrastructures.operation.sequence') or 'New'
        result = super(Operation, self).create(vals)
        return result

    def action_cancel(self):
        if self.env.uid != self.infrastructures_id.manager.id and not self.env.user.has_group('base.group_system'): 
            raise ValidationError(_("Only the infrastructure manager can validate the cancellation of the work "))
        for rec in self:
            rec.create_return_voucher()
            rec.state = 'canceled'
            
    def action_check(self):
        for rec in self:
            rec_products_id = rec.article_line_ids.mapped('product_id').mapped('id')
            products_line_ids = self.env['product.product'].search([('id', 'in', rec_products_id)])
            unavailable_number = 0
            for line in products_line_ids:
                for product in rec.article_line_ids:
                    if product.product_id.id == line.id:
                        if product.product_qty <= line.qty_available:
                            product.state = 'available'
                        else:
                            unavailable_number += 1
                            product.state = 'unavailable'
            if unavailable_number == 0:
                rec.state = 'confirmed'

    def action_purchase_requisition(self):
        return self.open_operations_purchase_requisition(self)

    def action_begin(self):
        if self.env.uid != self.infrastructures_id.manager.id and not self.env.user.has_group('base.group_system'): 
            raise ValidationError(_("Only the infrastructure manager can validate the start of the work "))
        for rec in self:
            rec.create_release_voucher()
            rec.state='inprogress'

    def action_validate(self):
        if self.env.uid != self.infrastructures_id.manager.id and not self.env.user.has_group('base.group_system'): 
            raise ValidationError(_("Only the infrastructure manager can validate the end of the work "))
        for rec in self:
            rec.state = 'done'
    
    def action_continue(self):
        if self.env.uid != self.infrastructures_id.manager.id and not self.env.user.has_group('base.group_system'): 
                raise ValidationError(_("Only the infrastructure manager can validate the continuity of the work "))
        for rec in self:
            if rec.infrastructures_id.state != 'inprogress':
                raise ValidationError(_('Please continue the infrastructure first'))
            rec.state = 'inprogress'
    
    def create_release_voucher(self):
        source = 'opération "{}" '.format(self.name)
        release_voucher = {
            'partner_id': 1,
            'picking_type_id': self.stock_operation_type.id,
            'origin': source,
            'state': 'draft',
            'operation_id': self.id,
            'location_id': self.stock_operation_type.default_location_src_id.id,
            'location_dest_id': self.stock_operation_type.default_location_src_id.id,
            'picking_type': 'outgoing',
            'move_ids_without_package': self.get_default_stock_product()

        }
        release_voucher_created=self.env['stock.picking'].create(release_voucher)
        release_voucher_created.state = 'done'

    def create_return_voucher(self):
        source = 'opération "{}" '.format(self.name)
        name = 'return article from operation "{}"'.format(self.name)
        release_voucher_id = self.env['stock.picking'].search([('operation_id', '=', self.id),('state', '=', 'done')])
        return_voucher = {
            'partner_id': 1,
            'picking_type_id': self.stock_operation_type.id,
            'origin': source,
            'location_dest_id': self.stock_operation_type.default_location_src_id.id,
            'entry_voucher_id':release_voucher_id.id,
            'move_ids_without_package': self.get_return_stock_product(),
            'state': 'done',
        }
        release_voucher_id.state = 'cancel'
        return_voucher_created = self.env['return.voucher'].create(return_voucher)
        return_voucher_created.state = 'done'

    # set value to the one2many fields for the return voucher
    def get_return_stock_product(self):
        return_stock_article_ids = []
        for rec in self:
            for line in self.article_line_ids:
                return_stock_article_ids.append((0,0,{
                    'name' : line.product_id.name,
                    'product_id' : line.product_id.id,
                    'return_product_qty' : line.product_qty,
                    'product_uom_qty' : line.product_qty,
                    'product_uom' : line.product_id.uom_id.id,
                    'location_id': self.stock_operation_type.default_location_src_id.id,
                    'location_dest_id': self.stock_operation_type.default_location_src_id.id,
                    'state' : 'done'
                }))
        return return_stock_article_ids

    # set value to the one2many fields for the release voucher
    def get_default_stock_product(self):
        default_stock_article_ids = []
        for rec in self:
            for line in self.article_line_ids:
                default_stock_article_ids.append((0,0,{
                    'name' : line.product_id.name,
                    'product_id' : line.product_id.id,
                    'product_uom_qty' : line.product_qty,
                    'product_uom' : line.product_id.uom_id.id,
                    'location_id': self.stock_operation_type.default_location_src_id.id,
                    'location_dest_id': self.stock_operation_type.default_location_src_id.id,
                    'quantity_done':line.product_qty
                }))
                line.state = 'used'
        return default_stock_article_ids

class OperationArticle(models.Model):
    _name = 'infrastructures.operations.article'
    _description = 'Article for each operation'

    @api.constrains('product_qty')
    def check_product_qty(self):
        for rec in self:
            if rec.product_qty <= 0:
                raise ValidationError(_(" Product quantity must be positive "))

    product_id = fields.Many2one('product.product', string="Product")
    product_qty = fields.Integer(string="Demand")
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account', required=True)
    state = fields.Selection([
        ('available', 'Available'),
        ('unavailable', 'Unavailable'),
        ('tocheck', 'Need to be checked'),
        ('used', 'Used')
        ],string="Product Status", readonly=True, default='tocheck')
    operation_id = fields.Many2one('infrastructures.operation', string="Operation")

class OperationWorker(models.Model):
    _name = 'infrastructures.operations.worker'
    _description = 'Worker for each operation'

    date = fields.Date(string='Date', required=True)
    worker_id=fields.Text(string='Name and firstname', required=True)
    start_time = fields.Float(string='Start time', required=True)
    end_time = fields.Float(string='End time', required=True)
    duration = fields.Float(string='Duration', compute="_compute_duration", readonly=True)
    activity = fields.Text(string="Activity", required = True)
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account', required=True)
    operation_id = fields.Many2one('infrastructures.operation', string="Operation")

    # activity = fields.Many2one('analitic.code', string = 'Activity' required=True)
    # analytic_code = fields.Many2one('analytic.code', string = 'Code' required=True)

    @api.depends('start_time', 'end_time')
    def _compute_duration(self):
        self.duration = self.end_time - self.start_time