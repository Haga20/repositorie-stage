from odoo import fields, api, models
class PurchaseOrder(models.Model):
    _inherit='purchase.order'
    operation_id = fields.Many2one('infrastructures.operation', string="Operation")