from odoo import fields, models, _


class PickingInherit(models.Model):
    _inherit = "stock.picking"

    operation_id = fields.Many2one('infrastructures.operation')
    location_id = fields.Many2one(
        'stock.location', "Source Location")
    location_dest_id = fields.Many2one(
        'stock.location', "Destination Location",
        default=lambda self: self.env['stock.picking.type'].browse(self.picking_type_id).default_location_dest_id,
        check_company=True, readonly=True, required=True,
        states={'draft': [('readonly', False)]})
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting Another Operation'),
        ('confirmed', 'Waiting'),
        ('assigned', 'Ready'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='Status', default="done")
    
