from odoo import models,fields,api, _
from odoo.exceptions import ValidationError

class Infrastructure(models.Model):
    _name = 'infrastructures.infrastructure'
    _description = ' infrastructure model'
    _inherit  =  ['mail.thread', 'mail.activity.mixin']

    def open_infrastructure_operations(self):
        return{
            'name':'Operations',
            'domain':[('infrastructures_id.id','=',self.id)],
            'view_type':'form',
            'res_model':'infrastructures.operation',
            'view_id':False, #(self.env.ref('infrastructure.infrastructures_operation_tree').id),
            'views':[(self.env.ref('infrastructure.infrastructures_operation_tree').id, 'tree'), (self.env.ref('infrastructure.operation_form').id, 'form')],
            'view_mode':'tree,form',
            'type':'ir.actions.act_window',
            'context': {
                "default_infrastructures_id": self.id,
            },
        }
    def open_infrastructure_worker(self):
        operation_ids = self.env['infrastructures.operation'].search([('infrastructures_id.id', '=', self.id)]).mapped('id')
        return{
            'name': _("Workers"),
            'domain':[('operation_id.id','in',operation_ids)],
            'view_type':'form',
            'res_model':'infrastructures.operations.worker',
            'view_id':False, 
            'view_mode':'tree,form',
            'type':'ir.actions.act_window',
            'context': {
                "group_by":['operation_id','date:day']
            }
        }
    def open_infrastructure_product(self):
        operation_ids = self.env['infrastructures.operation'].search([('infrastructures_id.id', '=', self.id)]).mapped('id')
        return{
            'name': _("Product needed"),
            'domain':[('operation_id.id','in',operation_ids)],
            'view_type':'form',
            'res_model':'infrastructures.operations.article',
            'view_id':False,
            'view_mode':'tree,form',
            'type':'ir.actions.act_window',
            'context': {
                "group_by":'operation_id'
            }
        }
    def get_operation_number(self):
        number = self.env['infrastructures.operation'].search_count([('infrastructures_id.id', '=', self.id)])
        self.operation_number = number
    
    sequence = fields.Char(string = "Infrastructure ID", required = True, copy = False,readonly = True, index = True, default = lambda self: 'New')
    name = fields.Char(string = 'Designation', required = True)
    description = fields.Text(string = 'Description')
    manager = fields.Many2one('res.users', string='Manager', required=True)
    category = fields.Selection([
        ('new', 'New'),
        ('existing', 'Existing')
    ],default = 'new', string  = 'Category')
    worker_type = fields.Selection([
        ('internal','Internal'),
        ('serviceprovider', 'Service Provider')
    ],default = 'internal', string = 'Worker type')
    infrastructures_images_ids = fields.One2many('infrastructures.image', 'infrastructure_id')
    state = fields.Selection([
        ('new','New'),
        ('inprogress','In progress'),
        ('done','Done'),
        ('canceled','Canceled'),
    ], string = 'state', readonly = True, default = 'new')
    operation_number = fields.Integer(string = 'Operations', compute = 'get_operation_number')
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.company)
    analytic_account_id = fields.Many2one('account.analytic.account', string='Analytic account', required=True)
    @api.model
    def create(self, vals):
        if vals.get('sequence','New') == 'New':
            vals['sequence'] = self.env['ir.sequence'].next_by_code('infrastructures.infrastructure.sequence') or 'New'
        result = super(Infrastructure, self).create(vals)
        return result  

    def action_cancel(self):
        if self.env.uid != self.manager.id and not self.env.user.has_group('base.group_system'):
            raise ValidationError(_("Only the infrastructure manager can validate the cancellation of the work "))
        for rec in self:
            operations_lines = self.env['infrastructures.operation'].search([('infrastructures_id', '=', rec.id)])
            for operation in operations_lines:
                if operation.state != 'done': 
                    operation.state = 'canceled'
            rec.state = 'canceled'

    def action_validate(self):
            if self.env.uid != self.manager.id and not self.env.user.has_group('base.group_system'):
                raise ValidationError(_("Only the infrastructure manager can validate the end of the work "))
            for rec in self:
                operation_not_done_list = []
                operations_lines = self.env['infrastructures.operation'].search([('infrastructures_id', '=', rec.id)])
                for operation in operations_lines:
                    if operation.state != 'done': 
                        operation_not_done_list.append(operation.name)
                if len(operation_not_done_list) > 0:
                    raise ValidationError(_('The operations "{}" are not done yet'.format(operation_not_done_list) ))
                rec.state = 'done'

    def action_begin(self):
        if self.env.uid != self.manager.id and not self.env.user.has_group('base.group_system'):
            raise ValidationError(_("Only the infrastructure manager or the admin can validate the start of the work "))
        for rec in self:
            rec.state = 'inprogress'
    
    def action_continue(self):
        if self.env.uid != self.manager.id and not self.env.user.has_group('base.group_system'):
                raise ValidationError(_("Only the infrastructure manager can validate the continuity of the work "))
        for rec in self:
            operations_lines = self.env['infrastructures.operation'].search([('infrastructures_id', '=', rec.id)])
            for operation in operations_lines:
                if operation.state != 'done': 
                    operation.state = 'inprogress'
            rec.state = 'inprogress'

    
    
    