# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Infrastructure',
    'version' : '1.1',
    'summary': 'Module for managing infrastructure',
    'sequence': 15,
    'description':"Manage all of your infrastructure",
    'category': 'Extra tools',
    'website': 'https://www.odoo.com/page/billing',
    'images' : [],
     'depends' : ['mail', 'product', 'hr', 'tozzi_purchase', 'analytic', 'tozzi_stock'],
    'data': [
        'data/infrastructure_sequence.xml',
        'data/infrastructures_image_sequence.xml',
        'data/operations_sequence.xml',
        'security/ir.model.access.csv',
        'views/infrastructures_infrastructure_views.xml',
        'views/infrastructures_operations_views.xml',
        'views/infrastructures_article_worker_views.xml',
        'views/purchase_requisition_views.xml',
        'views/stock_picking_views.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    
}
