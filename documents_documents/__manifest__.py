# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Documents - Document',
    'version': '1.0',
    'category': 'Operations/Documents',
    'summary': '',
    'description': """
Easily access your documents from fleet, stock, purchase and infrastructure.
""",
    'website': ' ',
    'depends': ['base','documents', 'fleet', 'stock', 'purchase', 'infrastructure', 'hr'],
    'data': [
        'data/documents_fleet_data.xml',
        'data/documents_infrastructure_data.xml',
        'data/documents_purchase_data.xml',
        'data/documents_stock_data.xml',
        'data/workflow_data.xml',
        'views/res_config_settings_views.xml',
        'views/fleet_vehicle_views.xml',
        'views/infrastructures_infrastructure_views.xml',
        'views/purchase_order_views.xml',
        'views/stock_picking_views.xml',
        'views/documents_views.xml',
        'security/ir.model.access.csv',
        'security/security.xml',

    ],
    'installable': True,
    'auto_install': True,
    'license': 'OEEL-1',
}
