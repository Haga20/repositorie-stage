# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResCompany(models.Model):
    _inherit = "res.company"

    def _domain_company(self):
        company = self.env.company
        return ['|', ('company_id', '=', False), ('company_id', '=', company)]

    # Fleet
    documents_fleet_settings = fields.Boolean()
    documents_fleet_folder = fields.Many2one('documents.folder', string="fleet Workspace", domain=_domain_company,
                                          default=lambda self: self.env.ref('documents_fleet.documents_fleet_folder',
                                                                            raise_if_not_found=False))

    # Infrastructure
    documents_infrastructure_settings = fields.Boolean()
    documents_infrastructure_folder = fields.Many2one('documents.folder', string="Infrastructure Workspace", domain=_domain_company,
                                          default=lambda self: self.env.ref('documents_infrastructure.documents_infrastructure_folder',
                                                                            raise_if_not_found=False))

    # Stock
    documents_stock_settings = fields.Boolean()
    documents_stock_folder = fields.Many2one('documents.folder', string="stock Workspace", domain=_domain_company,
                                          default=lambda self: self.env.ref('documents_stock.documents_stock_folder',
                                                                            raise_if_not_found=False))

    # Purchase
    documents_purchase_settings = fields.Boolean()
    documents_purchase_folder = fields.Many2one('documents.folder', string="Purchase Workspace", domain=_domain_company,
                                          default=lambda self: self.env.ref('documents_purchase.documents_purchase_folder',
                                                                            raise_if_not_found=False))

    # #GMO
    # documents_gmo_settings = fields.Boolean()
    # documents_gmo_folder = fields.Many2one('documents.folder', string="GMO Workspace", domain=_domain_company,
    #                                       default=lambda self: self.env.ref('documents_gmo.documents_gmo_folder',
    #                                                                         raise_if_not_found=False))

    # #MOT
    # documents_mot_settings = fields.Boolean()
    # documents_mot_folder = fields.Many2one('documents.folder', string="MOT Workspace", domain=_domain_company,
    #                                       default=lambda self: self.env.ref('documents_mot.documents_mot_folder',
    #                                                                         raise_if_not_found=False))

    # #Control de gestion (cdg)
    # documents_cdg_settings = fields.Boolean()
    # documents_cdg_folder = fields.Many2one('documents.folder', string="CDG Workspace", domain=_domain_company,
    #                                       default=lambda self: self.env.ref('documents_cdg.documents_cdg_folder',
    #                                                                         raise_if_not_found=False))
