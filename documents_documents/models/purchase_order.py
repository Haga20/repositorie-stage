from odoo import models

class PurchaseOrder(models.Model):
    _name = 'purchase.order'
    _inherit = ['purchase.order', 'documents.mixin']

    def _get_document_folder(self):
        return self.company_id.documents_purchase_folder

    def _get_document_owner(self):
        return self.env.user

    def _check_create_documents(self):
        return self.company_id.documents_purchase_settings and super()._check_create_documents()