from odoo import fields, models, _
from openerp import SUPERUSER_ID
from odoo.exceptions import ValidationError

class WorkflowActionRule(models.Model):
    _inherit = 'documents.workflow.rule'


    access_right_action = fields.Boolean(string="Action for access right")

    access_right_type = fields.Selection([
        ('public', 'Public'),
        ('restricted', 'Restricted'),
        ('uploader', 'Owner Only'),
    ], string='Access right', default='public')

    user_allowed_ids = fields.One2many('workflow.worker.permission', 'workflow_action_rule_id', string='User allowed')

    def apply_actions(self, document_ids):
        """
        called by the front-end Document Inspector to apply the actions to the selection of ID's.

        :param document_ids: the list of documents to apply the action.
        :return: if the action was to create a new business object, returns an action to open the view of the
                newly created object, else returns True.
        """
        documents = self.env['documents.document'].browse(document_ids)

        print("################### create id")
        print(documents.create_uid.id)
        print("###################user")
        print(self.env.user.id)
        print("################## admin")
        print(SUPERUSER_ID)
        if documents.create_uid.id != self.env.user.id and self.env.user.id != SUPERUSER_ID: 
             raise ValidationError(_("Only the uploader or the admin can apply an action"))
        # partner/owner/share_link/folder changes
        document_dict = {}
        if self.user_id:
            document_dict['owner_id'] = self.user_id.id
        if self.partner_id:
            document_dict['partner_id'] = self.partner_id.id
        if self.folder_id:
            document_dict['folder_id'] = self.folder_id.id
        if self.access_right_action:
            user_allowed_before = self.env['document.user.allowed'].search([('document_id', '=', documents.id)])
            document_dict['access_right_type'] = self.access_right_type
            if self.access_right_type == 'restricted':
                user_allowed_ids_list = []
                user_id_list=[]
                for line in self.user_allowed_ids:
                    id_dict={}
                    id_dict['user_id']=line.user.id
                    user_id_list.append(line.user.id)
                    user_allowed_ids_list.append((0,0,id_dict))
                for user in user_allowed_before:
                    if user.user_id.id not in user_id_list:
                        user.unlink()
                document_dict['user_allowed_ids'] = user_allowed_ids_list
            if self.access_right_type == 'public' or self.access_right_type == 'uploader':
                for user in user_allowed_before:
                    user.unlink()
                
        documents.write(document_dict)
        for document in documents:
            if self.remove_activities:
                document.activity_ids.action_feedback(
                    feedback="completed by rule: %s. %s" % (self.name, self.note or '')
                )

            # tag and facet actions
            for tag_action in self.tag_action_ids:
                tag_action.execute_tag_action(document)

        if self.activity_option and self.activity_type_id:
            documents.documents_set_activity(settings_record=self)

        if self.create_model:
            return self.create_record(documents=documents)

        return True
    
class WorkflowWorkerPermission(models.Model):
    _name = 'workflow.worker.permission'
    
    user = fields.Many2one('res.users', string='User', required=True)
    workflow_action_rule_id = fields.Many2one('documents.workflow.rule')

    