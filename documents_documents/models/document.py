from odoo import models, fields, api

class Document(models.Model):
    _inherit = 'documents.document'
    
    name = fields.Char('Name', copy=True, store=True, compute='_compute_name', inverse='_inverse_name', track_visibility="always")
    partner_id = fields.Many2one('res.partner', string="Contact", tracking=True, track_visibility="always")
    owner_id = fields.Many2one('res.users', default=lambda self: self.env.user.id, string="Owner",
                               tracking=True, track_visibility="always")
    folder_id = fields.Many2one('documents.folder',
                                string="Workspace",
                                ondelete="restrict",
                                tracking=True,
                                required=True,
                                index=True, 
                                track_visibility="always")
    tag_ids = fields.Many2many('documents.tag', 'document_tag_rel', string="Tags", track_visibility="always")
    user_allowed_ids = fields.One2many('document.user.allowed','document_id')
    user_ids = fields.Many2many('res.users', relation="aaa", compute='compute_user_allowed_ids', store=True)
    access_right_type = fields.Selection([
        ('public', 'Public'),
        ('restricted', 'Restricted'),
        ('uploader', 'Uploader Only'),
    ], string='Access right', default='public',track_visibility="always")

    @api.depends('user_allowed_ids')
    def compute_user_allowed_ids(self):
        for document in self:
            document.user_ids=document.user_allowed_ids.mapped('user_id').mapped('id')

class DocumentUserAllowed(models.Model):
    _name = 'document.user.allowed'

    user_id = fields.Many2one('res.users')

    document_id = fields.Many2one('documents.document')