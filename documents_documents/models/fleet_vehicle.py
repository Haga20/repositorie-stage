from odoo import models

class FleetVehicle(models.Model):
    _name = 'fleet.vehicle'
    _inherit = ['fleet.vehicle', 'documents.mixin']

    def _get_document_folder(self):
        return self.company_id.documents_fleet_folder

    def _get_document_owner(self):
        return self.env.user

    def _check_create_documents(self):
        return self.company_id.documents_fleet_settings and super()._check_create_documents()