# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'


    # Fleet

    documents_fleet_settings = fields.Boolean(
        related='company_id.documents_fleet_settings', readonly=False, string="Fleet")
    documents_fleet_folder = fields.Many2one(
        'documents.folder', related='company_id.documents_fleet_folder', readonly=False, string="fleet default workspace")

    @api.onchange('documents_fleet_folder')
    def _onchange_documents_fleet_folder(self):
        # Implemented in other documents-fleet bridge modules
        pass
    

    # Infrastructure

    documents_infrastructure_settings = fields.Boolean(
        related='company_id.documents_infrastructure_settings', readonly=False, string="Infrastructure")
    documents_infrastructure_folder = fields.Many2one(
        'documents.folder', related='company_id.documents_infrastructure_folder', readonly=False, string="infrastructure default workspace")

    @api.onchange('documents_infrastructure_folder')
    def _onchange_documents_infrastructure_folder(self):
        # Implemented in other documents-infrastructure bridge modules
        pass


    # Stock

    documents_stock_settings = fields.Boolean(
        related='company_id.documents_stock_settings', readonly=False, string="Stock")
    documents_stock_folder = fields.Many2one(
        'documents.folder', related='company_id.documents_stock_folder', readonly=False, string="stock default workspace")

    @api.onchange('documents_stock_folder')
    def _onchange_documents_stock_folder(self):
        # Implemented in other documents-hr bridge modules
        pass
    

    # purchase

    documents_purchase_settings = fields.Boolean(
        related='company_id.documents_purchase_settings', readonly=False, string="Purchase")
    documents_purchase_folder = fields.Many2one(
        'documents.folder', related='company_id.documents_purchase_folder', readonly=False, string="Purchase default workspace")

    @api.onchange('documents_purchase_folder')
    def _onchange_documents_purchase_folder(self):
        # Implemented in other documents-hr bridge modules
        pass

    # # GMO

    # documents_gmo_settings = fields.Boolean(
    #     related='company_id.documents_gmo_settings', readonly=False, string="gmo")
    # documents_gmo_folder = fields.Many2one(
    #     'documents.folder', related='company_id.documents_gmo_folder', readonly=False, string="gmo default workspace")

    # @api.onchange('documents_gmo_folder')
    # def _onchange_documents_gmo_folder(self):
    #     # Implemented in other documents-gmo bridge modules
    #     pass


    # # MOT

    # documents_mot_settings = fields.Boolean(
    #     related='company_id.documents_mot_settings', readonly=False, string="mot")
    # documents_mot_folder = fields.Many2one(
    #     'documents.folder', related='company_id.documents_mot_folder', readonly=False, string="mot default workspace")

    # @api.onchange('documents_mot_folder')
    # def _onchange_documents_mot_folder(self):
    #     # Implemented in other documents-mot bridge modules
    #     pass

    
    # # Contrôle de gestion(cdg)

    # documents_cdg_settings = fields.Boolean(
    #     related='company_id.documents_cdg_settings', readonly=False, string="Control de gestion")
    # documents_cdg_folder = fields.Many2one(
    #     'documents.folder', related='company_id.documents_cdg_folder', readonly=False, string="Control de gestion default workspace")

    # @api.onchange('documents_cdg_folder')
    # def _onchange_documents_cdg_folder(self):
    #     # Implemented in other documents-hr bridge modules
    #     pass