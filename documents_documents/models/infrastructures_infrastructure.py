
from odoo import models

class Infrastructure(models.Model):
    _name = 'infrastructures.infrastructure'
    _inherit = ['infrastructures.infrastructure', 'documents.mixin']

    def _get_document_folder(self):
        return self.company_id.documents_infrastructure_folder

    def _get_document_owner(self):
        return self.env.user

    def _check_create_documents(self):
        return self.company_id.documents_infrastructure_settings and super()._check_create_documents()