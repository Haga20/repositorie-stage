from odoo import models

class PickingType(models.Model):
    _name = 'stock.picking.type'
    _inherit = ['stock.picking.type', 'documents.mixin']

    def _get_document_folder(self):
        return self.company_id.documents_stock_folder

    def _get_document_owner(self):
        return self.env.user

    def _check_create_documents(self):
        return self.company_id.documents_stock_settings and super()._check_create_documents()