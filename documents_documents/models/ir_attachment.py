from odoo import models, _
from odoo.exceptions import ValidationError
from openerp import SUPERUSER_ID
class IrAttachment(models.Model):
    _inherit = 'ir.attachment'
    
    def unlink(self):
        if self.create_uid.id != self.env.user.id and self.env.user.id != SUPERUSER_ID: 
             raise ValidationError(_("Only the uploader or the admin can remove this attachment"))
        if not self:
            return True
        self.check('unlink')

        # First delete in the database, *then* in the filesystem if the
        # database allowed it. Helps avoid errors when concurrent transactions
        # are deleting the same file, and some of the transactions are
        # rolled back by PostgreSQL (due to concurrent updates detection).
        to_delete = set(attach.store_fname for attach in self if attach.store_fname)
        res = super(IrAttachment, self).unlink()
        for file_path in to_delete:
            self._file_delete(file_path)

        return res